import "react-vis/dist/style.css";

import { ThemeProvider, createGlobalStyle } from "styled-components";

import type { AppProps } from "next/app";

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    font-family: ${({ theme }) => theme.font.poppins};
  }
`;
function MyApp({ Component, pageProps }: AppProps) {
  const theme = {
    font: {
      poppins: "Poppins",
      rubik: "Rubik",
    },
    color: {
      green: "#07955B",
      blue: "#3200BB",
      grey: "#868686",
    },
  };

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <Component {...pageProps} />
    </ThemeProvider>
  );
}

export default MyApp;
