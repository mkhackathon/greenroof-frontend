import React, { ReactElement, useState } from "react";

import Card from "atoms/Card";
import styled from "styled-components";

const BenefitsStyle = styled.div``;
BenefitsStyle.displayName = "BenefitsStyle";

const Banner = styled.div`
  margin-top: 80px;

  width: 100%;
  height: 80vh;

  background: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)),
    url("/images/roof.jpg");
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;

  color: white;

  display: grid;

  > :not(:last-child) {
    cursor: pointer;
    height: 200px;
  }

  > :last-child {
    width: 75vw;
    height: 600px;
  }
`;
Banner.displayName = "Banner";

export default function Benefits(): ReactElement {
  const [selected, setSelected] = useState(0);

  return (
    <BenefitsStyle>
      <Banner>
        <Card
          selected={selected === 0}
          onClick={() => setSelected(0)}
          noShadow
          display="1oC"
          title="Environmental change"
        />
        <Card
          selected={selected === 1}
          onClick={() => setSelected(1)}
          noShadow
          display="6.7"
          title="Energy reduction"
        />
        <Card
          selected={selected === 2}
          onClick={() => setSelected(2)}
          noShadow
          display="62-73"
          title="Water retention"
        />
        <Card noShadow display="TBC"></Card>
      </Banner>
    </BenefitsStyle>
  );
}
