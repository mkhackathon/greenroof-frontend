import React, { ReactElement } from "react"

export default function About(): ReactElement {
  return <div>What is a green roof and why is it awesome?</div>
}
