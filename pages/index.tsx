import Button, { SocialButton } from "atoms/Buttons"
import React, { ReactElement } from "react"
import Link from "next/link"

import { BsArrowDown } from "react-icons/bs"
import Header from "organisms/Header"
import styled from "styled-components"
import Footer from "organisms/Footer"
import BenefitSection from "organisms/BenefitSection"
import WhatIsSection from "organisms/WhatIsSection"

const Container = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: 5vh;
  align-items: center;
  justify-content: center;
  text-align: center;

  color: black;
`
const Row = styled.div`
  padding: 0 10vw 0 10vw;
`

const Banner = styled.div`
  position: relative;
  width: 100%;
  height: 70vh;

  background: linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)),
    url("/images/roof.jpg");
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;

  color: white;

  display: flex;
  flex-flow: column;
  justify-content: space-between;
  align-items: center;

  box-sizing: border-box;
  padding: 150px 0;

  > svg {
    position: absolute;
    bottom: 0;
    left: 0;
  }
`
Banner.displayName = "Banner"

const BannerBody = styled.div`
  z-index: 1;

  > h1 {
    text-transform: uppercase;
  }

  > h4 {
    font: normal normal normal 18px/30px Poppins;
    letter-spacing: 0px;
    color: #9c9c9c;
  }
`
BannerBody.displayName = "BannerBody"

const ButtonChildren = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  column-gap: 0.5rem;
  margin-left: 1rem;

  > svg {
    display: inline-block;
  }
`
ButtonChildren.displayName = "ButtonChildren"

const Socials = styled.div`
  display: flex;
  justify-content: center;
  column-gap: 1rem;
  margin-top: 3rem;
`
Socials.displayName = "Socials"

export default function Index(): ReactElement {
  function handleLearnMoreClick() {
    document.getElementById("whatIsSection")?.scrollIntoView({
      behavior: "smooth",
    })
  }

  return (
    <Container>
      <Header />
      <Banner>
        <svg viewBox="0 0 100 8" xmlns="http://www.w3.org/2000/svg">
          <path
            d="M 100 0 L 100 10 L 0 10 L 0 0 C 30 10, 70 10, 100 0"
            stroke="white"
            strokeWidth="0.01"
            fill="white"
          />
        </svg>
        <BannerBody>
          <h1>
            IMPROVE YOUR ENVIRONMENT
            <br />
            WITH GREEN ROOFS
          </h1>
          <h4>
            Learn more about green roofs and their impact on the environment
          </h4>
          <Button onClick={handleLearnMoreClick}>
            <ButtonChildren>
              Learn more
              <BsArrowDown size="1.75rem" />
            </ButtonChildren>
          </Button>
          <Socials>
            <Link href="https://www.instagram.com/greenroofproject/">
              <SocialButton type="instagram" />
            </Link>
            <Link href="https://www.facebook.com/greenroofmk/">
              <SocialButton type="facebook" />
            </Link>
            <Link href="https://twitter.com/greenroofmk">
              <SocialButton type="twitter" />
            </Link>
          </Socials>
        </BannerBody>
      </Banner>
      <Row id="whatIsSection">
        <WhatIsSection />
      </Row>
      <Row>
        <BenefitSection />
      </Row>
      <Footer />
    </Container>
  )
}
