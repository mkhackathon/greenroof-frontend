import React, { ReactElement } from "react";

import styled from "styled-components";

const Option = styled.div`
  font-family: ${(props) => props.theme.font.poppins};
  color: ${(props) => props.theme.color.grey};
  line-height: 2rem;
  font-size: 14px;

  :nth-child(odd) {
    background: whitesmoke;
  }
`;

interface ListProps {
  options: string[];
}

export default function List({ options }: ListProps): ReactElement {
  return (
    <div>
      {options.map((val, id) => (
        <Option key={id}>{val}</Option>
      ))}
    </div>
  );
}
