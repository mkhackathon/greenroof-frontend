import React, { ReactElement } from "react"
import styled from "styled-components"

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`

const Label = styled.div`
  color: ${(props) => props.theme.color.grey};
  font-family: ${(props) => props.theme.font.poppins};
`

const Data = styled.div`
  position: relative;
  color: ${(props) => props.theme.color.green};
  font-family: ${(props) => props.theme.font.rubik};
  font-size: 4.5rem;
  font-weight: bold;

  > div {
    position: absolute;
    font-size: 1.25rem;
    font-weight: normal;
    right: -1.25rem;
    top: 0.75rem;
  }
`

interface DataSummaryProps {
  label: string
  data: number
  symbol: string
}

export default function DataSummary({
  label,
  data,
  symbol,
}: DataSummaryProps): ReactElement {
  return (
    <Container>
      <Label>{label}</Label>
      <Data>
        {data}
        <div>{symbol}</div>
      </Data>
    </Container>
  )
}
