import React, { ReactElement, ReactNode } from "react"

import styled from "styled-components"

interface StyledCardProps {
  size?: {
    width?: string
    height?: string
  }
  noShadow?: boolean
  selected?: boolean
}

const StyledCard = styled.div`
  background-color: #fff;
  color: black;

  height: ${({ size }: StyledCardProps) => size?.height ?? `auto`};
  width: ${({ size }: StyledCardProps) => size?.width ?? `25vw`};
  border-radius: 8px;
  ${({ noShadow }) => !noShadow && "box-shadow: 0 0 50px #e6e6e6"};
  box-sizing: border-box;

  padding: 30px;

  ${({ selected }) => selected === false && "opacity: 0.5"};
`

const Display = styled.span`
  color: ${({ theme }) => theme.color.green};
  font-size: 2rem;
`
Display.displayName = "Display"

const Title = styled.span`
  color: ${({ theme }) => theme.color.grey};
`
Title.displayName = "Title"

interface CardProps {
  size?: {
    width?: string
    height?: string
  }
  className?: string
  children?: ReactNode
  noShadow?: boolean
  selected?: boolean
  title?: ReactNode
  display?: ReactNode
  onClick?: () => void
}

export default function Card({
  size,
  className,
  children,
  noShadow,
  selected,
  title,
  display,
  onClick,
}: CardProps): ReactElement {
  return (
    <StyledCard {...{ size, selected, noShadow, className, onClick }}>
      {title && <Title>{title}</Title>}
      {display && <Display>{display}</Display>}
      {children}
    </StyledCard>
  )
}
