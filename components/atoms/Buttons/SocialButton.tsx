import React, { ReactElement } from "react"
import { RiFacebookFill, RiInstagramFill, RiTwitterFill } from "react-icons/ri"
import styled from "styled-components"

const StyledButton = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: transparent;
  color: white;
  height: 3.5rem;
  width: 3.5rem;
  outline: none;
  cursor: pointer;
  border: 1px solid rgba(140, 140, 140, 0.3);
  border-radius: 100%;
`

type IconType = "facebook" | "instagram" | "twitter"

interface SocialButtonProps {
  type: IconType
  iconSize?: string
  onClick?: () => void
}

export default function SocialButton({
  type,
  iconSize = "1rem",
  onClick,
}: SocialButtonProps): ReactElement {
  return (
    <StyledButton onClick={onClick}>
      {type === "facebook" ? (
        <RiFacebookFill size={iconSize} />
      ) : type === "instagram" ? (
        <RiInstagramFill size={iconSize} />
      ) : (
        <RiTwitterFill size={iconSize} />
      )}
    </StyledButton>
  )
}
