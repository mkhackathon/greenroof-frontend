import React, { ReactElement, ReactNode } from "react"
import styled from "styled-components"

const StyledButton = styled.button`
  background-color: ${(props) => props.theme.color.green};
  color: white;
  font-family: ${(props) => props.theme.font.poppins};
  height: 3rem;
  border-radius: 8px;
  outline: none;
  cursor: pointer;
  border: 1px solid ${(props) => props.theme.color.green};
  transition: all 100ms ease-in-out;
  box-shadow: 0 5px 15px -5px ${(props) => props.theme.color.green};
  padding: 0.5rem 1rem 0.5rem 1rem;

  :active {
    background-color: white;
    color: ${(props) => props.theme.color.green};
  }
`

interface ButtonProps {
  children: ReactNode
  className?: string
  onClick?: () => void
}

export default function Button({
  children,
  className,
  onClick,
}: ButtonProps): ReactElement {
  return (
    <StyledButton className={className} onClick={onClick}>
      {children}
    </StyledButton>
  )
}
