import React, { ReactElement, ReactNode } from "react"
import styled from "styled-components"
import Card from "../../atoms/Card"
import DataSummary from "../../atoms/DataSummary"

const Container = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: 2rem;
  padding: 3vh 0 3vh 0;
`

interface DataCardProps {
  label: string
  data: number
  symbol: string
  children?: ReactNode
}

export default function DataCard({
  label,
  data,
  symbol,
  children,
}: DataCardProps): ReactElement {
  return (
    <Card>
      <Container>
        <DataSummary label={label} data={data} symbol={symbol} />
        {children}
      </Container>
    </Card>
  )
}
