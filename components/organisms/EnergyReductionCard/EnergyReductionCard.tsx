import React, { ReactElement } from "react";

import styled from "styled-components";

const EnergyReduction = styled.div`
  display: flex;
`;
EnergyReduction.displayName = "EnergyReduction";

export const MainSection = styled.div``;
MainSection.displayName = "MainSection";

const SideSection = styled.div``;
SideSection.displayName = "SideSection";

export default function EnergyReductionCard(): ReactElement {
  return (
    <EnergyReduction>
      <MainSection>
        <h2>Our green roof in Saxon Court</h2>
      </MainSection>
      <SideSection></SideSection>
    </EnergyReduction>
  );
}
