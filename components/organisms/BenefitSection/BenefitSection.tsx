import { LineSeries, XAxis, XYPlot, YAxis } from "react-vis";
import React, { ReactElement } from "react";

import Button from "atoms/Buttons";
import Card from "atoms/Card";
import DataCard from "molecules/DataCard";
import List from "atoms/List";
import styled from "styled-components";

const Container = styled.div`
  max-width: 1100px;
  display: flex;
  flex-direction: column;
  row-gap: 40px;
  align-items: center;
  justify-content: center;
`;

const Label = styled.div`
  font-family: ${(props) => props.theme.font.poppins};
  font-size: 3rem;
`;

const Description = styled.div`
  font-family: ${(props) => props.theme.font.poppins};
  color: ${(props) => props.theme.color.grey};
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
  column-gap: 2vw;
  width: 100%;
  align-items: center;
  justify-content: center;
`;

const ChartCard = styled(Card)`
  width: 100%;

  > span:first-child {
  }
`;

const ButtonChildren = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  column-gap: 0.5rem;
  margin-left: 1rem;

  > svg {
    display: inline-block;
  }
`;
ButtonChildren.displayName = "ButtonChildren";

export default function BenefitSection(): ReactElement {
  const cardOptions = [
    ["Warming reduction", "Air cooling", "Air quality"],
    [
      "Saves on heating bills",
      "Absorbs heat on hot days",
      "Insulates in the winter",
    ],
    [
      "Collects rain water",
      "Reduces rain water on the ground",
      "Self-watering",
    ],
  ];

  const data = [
      { x: 0, y: 8 },
      { x: 1, y: 5 },
      { x: 2, y: 4 },
      { x: 3, y: 9 },
      { x: 4, y: 1 },
      { x: 5, y: 7 },
      { x: 6, y: 6 },
      { x: 7, y: 3 },
      { x: 8, y: 2 },
      { x: 9, y: 0 },
    ],
    data1 = [
      { x: 0, y: 3 },
      { x: 1, y: 7 },
      { x: 2, y: 3 },
      { x: 3, y: 6 },
      { x: 4, y: 1 },
      { x: 5, y: 4 },
      { x: 6, y: 8 },
      { x: 7, y: 3 },
      { x: 8, y: 6 },
      { x: 9, y: 3 },
    ];

  return (
    <Container>
      <Label>What are the benefits?</Label>
      <Description>
        Many desktop publishing packages and web page editors now use Lorem
        Ipsum as their default model text, and a search for lorem ipsum will
        uncover many web sites still in their infancy.
      </Description>
      <Row>
        <DataCard label="Environmental change" data={-1} symbol="°C">
          <List options={cardOptions[0]} />
        </DataCard>
        <DataCard label="Energy reduction" data={6.7} symbol="%">
          <List options={cardOptions[1]} />
        </DataCard>
        <DataCard label="Water retention from" data={62} symbol="%">
          <List options={cardOptions[2]} />
        </DataCard>
      </Row>
      <Row>
        <ChartCard>
          <h1>Our green roof in Saxon Court</h1>
          <XYPlot height={200} width={400}>
            <LineSeries data={data} />
            <LineSeries data={data1} />
            <XAxis
              title="Time"
              hideTicks
              // @ts-ignore
              position="middle"
              style={{
                title: { transform: "translateX(85px) translateY(12px)" },
              }}
            />
            <YAxis
              title="Temp"
              hideTicks
              // @ts-ignore
              position="middle"
              style={{
                title: { transform: "translateX(-12px) translateY(40px)" },
              }}
            />
          </XYPlot>
        </ChartCard>
      </Row>
      <Button>See our data</Button>
    </Container>
  );
}
