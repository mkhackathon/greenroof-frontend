import React, { ReactElement } from "react"
import styled from "styled-components"

const Container = styled.div`
  height: 7vh;
  width: 100vw;
  background-color: #fff;
  display: grid;
  grid-template-columns: 2vw 15vw 55vw 26vw 2vw;
  place-items: center;

  > img {
    height: 2rem;
  }
`

const Text = styled.div`
  font-family: ${(props) => props.theme.font.poppins};
  font-size: 0.7rem;
  line-height: 7vh;
  width: 100%;
  padding-left: 1rem;
`

export default function Footer(): ReactElement {
  return (
    <Container>
      <div />
      <img src="images/MKH-Logo-Black.png" alt="MKH Logo" />
      <Text style={{ textAlign: "left" }}>
        In collaboration with: Pooleyville, Made in MK, MK Geek Night,
        Pooleyville and Milton Keynes Council
      </Text>
      <Text style={{ textAlign: "right" }}>Design template by UGURATES</Text>
      <div />
    </Container>
  )
}
