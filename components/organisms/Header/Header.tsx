import React, { ReactElement } from "react"

import styled from "styled-components"

const HeaderStyle = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  z-index: 1000;

  width: 95vw;
  height: 100px;

  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;

  padding: 0 3vw 0 3vw;

  > img {
    height: 50px;
  }
`
HeaderStyle.displayName = "HeaderStyle"

const Links = styled.div`
  display: flex;
  flex-direction: row;
  color: white;
  column-gap: 1.5rem;
`
const Text = styled.div`
  font-size: 1rem;
`

export default function Header(): ReactElement {
  return (
    <HeaderStyle>
      <img src="images/logo-white.png" alt="Green Roof Logo" />
      <Links>
        <Text>What is a green roof?</Text>
        <Text>The benefits</Text>
        <Text>Get involved</Text>
      </Links>
    </HeaderStyle>
  )
}
