import Card from "atoms/Card"
import React, { ReactElement } from "react"
import styled from "styled-components"

const Container = styled.div`
  display: grid;
  grid-template-rows: auto;
  text-align: left;
  padding: 1rem 1rem 1.5rem 1rem;
`

const Row = styled.div`
  display: flex;
  flex-direction: row;
  column-gap: 3rem;

  > img {
    align-self: center;
    justify-content: right;
    height: 7rem;
  }
`

const Label = styled.div`
  font-family: ${(props) => props.theme.font.poppins};
  font-size: 2rem;
`

const Body = styled.div`
  font-family: ${(props) => props.theme.font.poppins};
  font-size: 1rem;
  color: ${(props) => props.theme.color.grey};
  margin-top: 1.5rem;
`

export default function WhatIsSection(): ReactElement {
  return (
    <Card size={{ width: "100%", height: "auto" }}>
      <Container>
        <Label>What is a green roof?</Label>
        <Row>
          <Body>
            A Green Roof (also referred to as a vegetative, living or eco roof)
            is a layer of vegetation that covers part or whole of a roof. They
            serve several purposes for a building, such as absorbing rainwater,
            providing insulation and creating a habitat for wildlife. A Green
            Roof is built up of several layers, each of which plays its part in
            supporting the vegetation.
          </Body>
          <img src="images/cross-section-1.png" alt="cross-section" />
        </Row>
      </Container>
    </Card>
  )
}
