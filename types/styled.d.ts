// import original module declarations
import "styled-components"

// and extend them!
declare module "styled-components" {
  export interface DefaultTheme {
    font: {
      poppins: string
      rubik: string
    }
    color: {
      green: string
      blue: string
      grey: string
    }
  }
}
